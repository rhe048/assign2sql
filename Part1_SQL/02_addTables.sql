CREATE TABLE SUPERHERO (
	ID int IDENTITY(1, 1) PRIMARY KEY,
	Name nvarchar(30) NOT NULL,
	Alias nvarchar(30),
	Origin nvarchar(100),
);

CREATE TABLE ASSISTANT (
	ID int IDENTITY(1, 1) PRIMARY KEY,
	Name nvarchar(30),
);

CREATE TABLE POWER (
	ID int IDENTITY(1, 1) PRIMARY KEY,
	Name nvarchar(30) NOT NULL,
	Description nvarchar(100),
);