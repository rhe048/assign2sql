
INSERT INTO POWER (Name, Description)
VALUES ('Superstrength', 'Strength in the uppermost quintiles of humans'),
('Wealth', 'An adaptable superpower granting the ability to do many things'),
('Web slinging', 'The abiltiy to shoot sticky webs from extremities'),
('Laser vision', 'Shoots a laser beam from their eyes'),
('Flight', 'The ability to fly');


INSERT INTO HEROPOWERS (SuperheroID, PowerID)
VALUES ((SELECT ID FROM SUPERHERO WHERE Name='Superman'), (SELECT ID FROM POWER WHERE Name='Superstrength')),
((SELECT ID FROM SUPERHERO WHERE Name='Superman'), (SELECT ID FROM POWER WHERE Name='Laser vision')),
((SELECT ID FROM SUPERHERO WHERE Name='Superman'), (SELECT ID FROM POWER WHERE Name='Flight')),
((SELECT ID FROM SUPERHERO WHERE Name='Batman'), (SELECT ID FROM POWER WHERE Name='Wealth')),
((SELECT ID FROM SUPERHERO WHERE Name='Spiderman'), (SELECT ID FROM POWER WHERE Name='Superstrength')),
((SELECT ID FROM SUPERHERO WHERE Name='Spiderman'), (SELECT ID FROM POWER WHERE Name='Web slinging'));

